%%%---------------------------------------------------%%%
%%%-----Post processing for roughness calculation-----%%%
%%%-----------version: 1.1 date: 20/04/2020-----------%%%
%%% The code is distributed under a BSD license       %%%
%%%---------------e-mail:e.e.asik@tue.nl--------------%%%
%%%-----------------------------------by Erkan Asik---%%%
%%%---------------------------------------------------%%%
%%%Change log:----------------------------------------%%%
%%% - Interpolation of missing data                   %%%
%%% - Correction in the aspect ratio of surface plot  %%% 
%%% - User interaction in selection of filters        %%%
%%% - Zero order Gaussian regression filter           %%%
%%%---------------------------------------------------%%%


% format shortE;
clc;
clear all;
fileid = uigetfile('*.dat');
Data = importdata(fileid);
% Data = importdata('02_L_MC_1.dat');  % 
% data_1=importdata('dat3_Ra.dat');
% data_2=importdata('dat3_Wa.dat');
% data_3=importdata('dat3_Wab.dat');

%Find and arrange the coordinates from the data
ver=unique(Data(:,2,:));  
hor=unique(Data(:,1,:));

Z=reshape(Data(:,3),size(hor,1),size(ver,1));
Z=Z';

%line 10-11 can also be done in a for loop
% Z=zeros(size(ver,1),size(hor,1));
% for i=1:size(A,1)
%   j=find(ver==A(i,2));
%   l=find(hor==A(i,1));
%   Z(j,l)=A(i,3);
% end

Z(Z>1000)=NaN; % remove the unindexed values
Z=fillmissing(Z,'linear',2,'EndValues','nearest'); %interpolate missing data

% baseline_fit_order=3;   %order of the polynomial for line fitting
% line_position=230.74;    %in microns
line_orientation=2;     % 1 for vertical 2 for horizontal

if line_orientation==1
    prompt= {['Line position:   max value  ',num2str(max(hor))]};
elseif line_orientation==2
    prompt= {['Line position:   max value  ',num2str(max(ver))]};
end
dlgtitle = 'Input';
dims = [1 50];
definput = {'117.86;231.57;345.28'};   %Default filter values
Line_p = inputdlg(prompt,dlgtitle,dims,definput); %input for filter values
line_position_all=sortline(Line_p{1});
        
        
%wavelength cut off values for of the gauss filters 
% % lambda_roughness=0.08;   %in mm
% % lambda_waviness_1=0.08;  %in mm
% % lambda_waviness_2=0.25; %in mm

prompt = {'Fitting curve order:','lambda_roughness cut off:  (in mm)','lambda_waviness_1 cut off:  (in mm)','lambda_waviness_2 cut off:  (in mm)'};
dlgtitle = 'Input';
dims = [1 50];
definput = {'3','0.08','0.08','0.25'};   %Default filter values
SoWhat = inputdlg(prompt,dlgtitle,dims,definput); %input for filter values
baseline_fit_order=str2double(SoWhat{1});   %order of the polynomial for line fitting
%wavelength cut off values for of the gauss filters 
lambda_roughness=str2double(SoWhat{2});   %in mm
lambda_waviness_1=str2double(SoWhat{3});  %in mm
lambda_waviness_2=str2double(SoWhat{4}); %in mm


for ind=1:size(line_position_all,2)
line_position=line_position_all(ind);
if line_orientation==1
    [devi,pixloc]=min(abs(hor-line_position));
    selected_line=flip(Z(:,pixloc));
    coor=ver; 
elseif line_orientation==2
    hl_loc=max(ver)-line_position; % to be consisten with the plu software
    [devi,pixloc]=min(abs(ver-hl_loc));
    selected_line=Z(pixloc,:)';
    coor=hor;
else
    fprintf('invalid line orientation');    
end

%fit a polynome to the line with specified order
[p1,fitparam] = polyfit(coor,selected_line,baseline_fit_order);
R2=1-(fitparam.normr/norm(selected_line - mean(selected_line)))^2; %calculate Rsquared value
fitted_line=polyval(p1,coor);
corrected_line=selected_line-fitted_line;
%detrend does the same thing with previous lines keep in mind for shorter code. 
%corrected_line=detrend(selected_line,baseline_fit_order); 

[rr1,corrected_line]=filtGausslowZero(corrected_line,coor,0.003*(coor(3)-coor(2)));
[roughness1,waviness1,fil_size1]=filtGausslowZero(corrected_line,coor,lambda_roughness);
[Rk,Rpk,Rvk,Mr1,Mr2,Rms]=bac(roughness1);
[roughness2,waviness2,fil_size2]=filtGausslowZero(corrected_line,coor,lambda_waviness_1);
[roughness3,waviness3,fil_size3]=filtGausslowZero(corrected_line,coor,lambda_waviness_2);

%%-----------------------%%
%%----surface plot 2D----%%
%%-----------------------%%
figure;
subplot(2,3,1); h=surf(hor,ver,Z,'edgecolor', 'none'); box on
colormap('jet');
colorbar;
rotate3d on;
title('Surface Plot')
% xlabel('Distance (\mum)')
% ylabel('Distance (\mum)')
view(0,90);
axis ij
xlim([min(hor),max(hor)])
ylim([min(ver),max(ver)])
xticks ([-10000 100000])
yticks ([-10000 100000])
hold on
pbaspect([size(hor,1) size(ver,1) 400])
if line_orientation==1
    xline(line_position,'k','LineWidth',0.75 );
elseif line_orientation==2
    yline(max(ver)-line_position,'k','LineWidth',0.75 );  
end

%Selected line and the fitted_curve
subplot(2,3,2); plot2dline(coor,selected_line,fitted_line); box on
% annotation('textbox',[0.5 0.33 0.3 0.3],'String',"R-squared= "+R2,'FitBoxToText','on');
title('Profile')
xlabel('Distance (\mum)')
ylabel('Height (\mum)')
%Corrected line
subplot(2,3,3); plot2dline(coor,corrected_line); box on
title('Roughness profile')
xlabel('Distance (\mum)')
ylabel('Height (\mum)')
%Roughness profile
subplot(2,3,4); plot2dline(coor,roughness1); box on
title('Roughness profile with filter')
xlabel('Distance (\mum)')
ylabel('Height (\mum)')

%Waviness profile_1
subplot(2,3,5); plot2dline(coor,waviness2); box on
title('Waviness profile with filter 1')
xlabel('Distance (\mum)')
ylabel('Height (\mum)')
%Waviness profile_2
subplot(2,3,6); plot2dline(coor,waviness3); box on
title('Waviness profile with filter 2')
xlabel('Distance (\mum)')
ylabel('Height (\mum)')
hold off
%%-----------------------%%
%%--END surface plot 2D--%%
%%-----------------------%%


%%-----------------------%%
%%--Calculate R values---%%
%%-----------------------%%
[l1_Ra,l1_RMS,l1_PV]=calcRa(corrected_line);
[r1_Ra(ind),r1_RMS(ind),r1_PV(ind),r1_Sk(ind),r1_K(ind)]=calcRa(roughness1);
[w2_Ra(ind),w2_RMS(ind),w2_PV(ind),w2_Sk(ind),w2_K(ind)]=calcRa(waviness2);
[w3_Ra(ind),w3_RMS(ind),w3_PV(ind),w3_Sk(ind),w3_K(ind)]=calcRa(waviness3);

%%-----------------------%%
%%----Print the Values---%%
%%-----------------------%%
% some fun with fprintf
% fprintf('    _______________________________________________________________ \n');
% fprintf('   |           Calculated Rougness and Waviness Values             | \n');
% fprintf('   |_______________________________________________________________| \n');
% fprintf('   | Filters |  Roughness:%4.2f | Waviness_1:%4.2f | Waviness_2:%4.2f | \n'...
%     ,lambda_roughness,lambda_waviness_1,lambda_waviness_2);
% fprintf('   |_______________________________________________________________| \n');
% fprintf('   |   Ra    |% 16.6f |% 16.6f |% 16.6f | \n'...
%     ,r1_Ra,w2_Ra,w3_Ra);
% fprintf('   |   PV    |% 16.6f |% 16.6f |% 16.6f | \n'...
%     ,r1_PV,w2_PV,w3_PV);
% fprintf('   |_______________________________________________________________| \n');
end
%%-----------------------%%
%%---Print the Values 2--%%
%%-----------------------%%
Results=zeros(size(line_position_all,2),16);
Names=strings(size(line_position_all,2),1);
for ind=1:size(line_position_all,2)
    Results(ind,1)=line_position_all(1,ind);
    
    Results(ind,2)=r1_Ra(ind);  %average
    Results(ind,3)=r1_RMS(ind); %rms
    Results(ind,4)=r1_PV(ind);  %peak-valley
    Results(ind,5)=r1_Sk(ind);  %skewness
    Results(ind,6)=r1_K(ind);   %kurtosis
    
    Results(ind,7)=w2_Ra(ind);
    Results(ind,8)=w2_RMS(ind);
    Results(ind,9)=w2_PV(ind);
    Results(ind,10)=w2_Sk(ind);
    Results(ind,11)=w2_K(ind);
    
    Results(ind,12)=w3_Ra(ind);
    Results(ind,13)=w3_RMS(ind);
    Results(ind,14)=w3_PV(ind);
    Results(ind,15)=w3_Sk(ind);
    Results(ind,16)=w3_K(ind);
    
    Names(ind,1)=fileid;
end
% Colnames={['Roughness: ',num2str(lambda_roughness)],...
%     ['Waviness_1: ',num2str(lambda_waviness_1)],['Waviness_2: ',num2str(lambda_waviness_2)]};
Colnames={'Line_Position','R_a','R_rms','R_pv','R_skw','R_kur',...
    'W2_a','W2_rms','W2_pv','W2_skw','W2_kur',...
    'W3_a','W3_rms','W3_pv','W3_skw','W3_kur'};
Colnames1={'File_Name'};
dummy_tab=table(Names,'VariableNames',Colnames1);
RR1=[r1_Ra;r1_PV];
RW2=[w2_Ra;w2_PV];
RW3=[w3_Ra;w3_PV];
Tabdata=array2table(Results,'VariableNames',Colnames);
Tabdata=[dummy_tab Tabdata]
% TabData = table(RR1,RW2,RW3,'RowNames',Rownames,'VariableNames',Colnames)
% uitable('Data',TabData{:,:},'ColumnName',T.Properties.VariableNames,...
%     'RowName',T.Properties.RowNames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);


function [P_Ra,P_RMS,P_PV,P_sk,P_k]=calcRa(R,varargin)
% Just a consideration for the edges for now keep off     
%    if nargin==2
%         if varargin{1,1}>size(R)/2
%             fprintf('Filter is too large. Roughness calculated over full length\n'); 
%         else
%             k=(varargin{1,1}-1)/4;
%             R=R(k+1:end-k);
%         end        
%     elseif (nargin~=2 & nargin~=1)
%         fprintf('too many input arguments in calcRa\n');
%     end
    if size(R,2)==1
        R=R-mean(R); % shift the profile   
        P_Ra=sum(abs(R(:))/size(R,1));        %average
        P_RMS=sqrt(sum(R(:).^2)/size(R,1));   %root mean square
        P_sk=(sum(R(:).^3)/size(R,1))/P_RMS^3;%skewness
        P_k=(sum(R(:).^4)/size(R,1))/P_RMS^4; %kurtosis
        P_PV=max(R)-min(R);
    else
        R=R-mean2(R);
        P_Ra=sum(abs(R(:))/(size(R,1)*size(R,2)));
        P_PV=max(max(R))-min(min(R));
        P_RMS=0;
        for cnti=1:size(R,1)
            for cntj=1:size(R,2)
                P_RMS=P_RMS+R(cnti,cntj)^2;
            end
        end
        P_RMS=sqrt(P_RMS/(size(R,1)*size(R,2)));
    end
end
function plot2dline(xin,yin,varargin)
    rmsup=mean(yin)+rms(yin);
    rmsdown=mean(yin)-rms(yin);
%     figure; 
    hold on
    plot(xin, yin,'k','LineWidth',1.5)
    plot(xin, rmsup*ones(size(yin,1),1),'r--','LineWidth',0.75)
    plot(xin, rmsdown*ones(size(yin,1),1),'r--','LineWidth',0.75)
    plot(xin, mean(yin)*ones(size(yin,1),1),'g--','LineWidth',0.75)
    grid on
    xlim([min(xin),max(xin)])
    ylim([min(yin)-abs(0.1*min(yin)),max(yin)+abs(0.1*max(yin))])

    if nargin>2
        for i=1:nargin-2
            plot(xin, varargin{1,i},'LineWidth',1.0)
        end
    end
end
function [roughness, meanline, filter_size]=filtGausslow(unfiltered,coor,lambdac)
%Based on Computational surface and roundness metrology
%DOI 10.1007/978-1-84800-297-5
    dcoor=(coor(2)-coor(1))/10^3; % in mm
    x=(-lambdac:dcoor:lambdac);
    alpha = sqrt(log(2)/pi ); %     alpha=0.4697;

% generate the Gaussian filter
    S=(1/(alpha*lambdac)).*exp(-pi*(x/(alpha*lambdac)).^2);
    S=S/sum(S); % normalize to unit sum
    
% % % %     alpha=3.67;
% % % %     S=(alpha/lambdac).*(2-alpha*(abs(x)/lambdac)).*exp(-alpha.*(abs(x)/lambdac));
% % % %     S=S/sum(S);

    %padding for the edge effects
    A=[flip(unfiltered);unfiltered;flip(unfiltered)];
    meanline=conv(A,S,'same'); % convolve z and S
    meanline=meanline(size(unfiltered,1)+1:2*size(unfiltered,1),1);
    meanline=meanline-mean(meanline);
    roughness=unfiltered-meanline;
    filter_size=size(x,2);
end
function [roughness, meanline, filter_size]=filtGausslowZero(unfiltered,coor,lambdac)
%Zero order gaussian regression
%Based on Computational surface and roundness metrology
%DOI 10.1007/978-1-84800-297-5
    dcoor=(coor(2)-coor(1))/10^3; % in mm
    x=(-lambdac:dcoor:lambdac);
    alpha = sqrt(log(2)/2/pi/pi);
    p=(1:1:size(coor,1))'; % For each position k (center of the filter), generate the filter over the entire profile length
    meanline=zeros(size(coor,1),1);
    for k=1:size(coor,1)        
        S = (1/sqrt(2*pi)/alpha/lambdac).*exp(-0.5*((k-p)*dcoor/alpha/lambdac).^2); % generate weighting function
        SMOD = S/sum(S); % normalize filter to unit sum
        meanline(k,1) = sum(SMOD.*unfiltered); % sum of products (this step replaces the convolution
    end
    meanline=meanline-mean(meanline);
    roughness=unfiltered-meanline;
    filter_size=size(x,2);
end
function line_position=sortline(a)
if a(end)==';'
    a=[';',a];
else
    a=[';',a,';'];
end
pos=strfind(a,';');
for i=1:size(pos,2)-1;
    line_position(i)=str2double(a(pos(i)+1:pos(i+1)-1));
end
end
function [Rk,Rpk,Rvk,Mr1,Mr2,Rms]=bac(profile)
maxp=max(profile);
minp=min(profile);
cnt=0;
Rms=0;
for i=maxp:-(maxp-minp)/100:minp
    cnt=cnt+1;
    A=profile>i;
    Rms(cnt,1)=(sum(A==1))/(size(profile,1)-1)*100;
%     Rms(cnt,2)=100*(1-(maxp-i)/(maxp-minp));
    Rms(cnt,2)=i;
end
AAA=gradient(Rms(:,1));
a=Rms(find(Rms(:,1)>30,1):find(Rms(:,1)>70,1),1);
b=Rms(find(Rms(:,1)>30,1):find(Rms(:,1)>70,1),2);

[p1,fitparam] = polyfit(a,b,1);
R2=1-(fitparam.normr/norm(Rms(31:71,1) - mean(Rms(31:71,1))))^2; %calculate Rsquared value
fitted_line=polyval(p1,0:1:100);

Mr1=Rms(find(Rms(:,2)<fitted_line(1),1),1);   %material ratio 1
Mr2=Rms(find(Rms(:,2)<fitted_line(end),1),1); %material ratio 2
Rk=fitted_line(1)-fitted_line(end);           %core profile depth
Rpk=maxp-fitted_line(1);                      %peak height
Rvk=minp-fitted_line(end);                    %valley depth

if isempty(Mr1); Mr1=0; end;
if isempty(Mr2); Mr2=100; end;

figure; hold on;
plot(Rms(:,1),Rms(:,2),'k','LineWidth',1.5)
plot(0:1:100,fitted_line,'r--','LineWidth',0.75)
% plot(AAA,Rms(:,2),'g--','LineWidth',0.75)

xlim([min(Rms(:,1)),max(Rms(:,1))])
ylim([min(Rms(:,2)),max(Rms(:,2))])
grid on
title('Material Ratio Curve')
xlabel('Material ratio (%)')
ylabel('Height (\mum)')
end